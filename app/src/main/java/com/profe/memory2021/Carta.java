package com.profe.memory2021;

public class  Carta {

    public enum Estat {FIXED, FRONT, BACK};
    private int backimage;
    private int frontimage;
    private Estat estat;

    public int getBackimage() {
        return backimage;
    }

    public void setBackimage(int backimage) {
        this.backimage = backimage;
    }

    public int getFrontimage() {
        return frontimage;
    }

    public void setFrontimage(int frontimage) {
        this.frontimage = frontimage;
    }

    public Estat getEstat() {
        return estat;
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }

    public Carta(int backimage, int frontimage, Estat estat) {
        this.backimage = backimage;
        this.frontimage = frontimage;
        this.estat = estat;
    }

    public Carta(int frontimage) {
        this.frontimage = frontimage;
        this.estat = Estat.BACK;
        this.backimage = R.drawable.back;


    }

    public int getImage() {

        int image;
        image = (estat == Estat.BACK) ? backimage: frontimage;
        return image;

    }

    public void girar()
    {

        switch (estat) {

            case BACK: estat = Estat.FRONT;
                       break;
            case FRONT: estat = Estat.BACK;
                       break;

            /* case FIXED no cal fer res */

        }



    }




}
