package com.profe.memory2021;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Carta> listaCartas;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CartaAdapter cartaAdapter;

    public int[] llistaImatges = {R.drawable.c0, R.drawable.c1, R.drawable.c2, R.drawable.c3,
    R.drawable.c4, R.drawable.c5, R.drawable.c6, R.drawable.c7, R.drawable.c8, R.drawable.c9,
    R.drawable.c10, R.drawable.c11};
    public int NUMERO_COLUMNAS = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seleccionarCartes();
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new GridLayoutManager (this, NUMERO_COLUMNAS);
        cartaAdapter = new CartaAdapter(this, listaCartas);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cartaAdapter);



    }

    public void seleccionarCartes() {
        listaCartas = new ArrayList();
        for (int i=0; i<llistaImatges.length; i++)
        {
            listaCartas.add(new Carta(llistaImatges[i]));
        }
        Collections.shuffle(listaCartas);
        for (int i=listaCartas.size()/2; i<listaCartas.size(); i++)
        {
            listaCartas.get(i).setFrontimage(listaCartas.get(i-listaCartas.size()/2).getFrontimage());



        }



    }
}