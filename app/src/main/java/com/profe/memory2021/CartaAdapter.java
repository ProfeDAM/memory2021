package com.profe.memory2021;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.ImageHolder> {

    private Context context;
    private ArrayList<Carta> listaCartas;

    public CartaAdapter(Context context, ArrayList<Carta> listaCartas) {
        this.context = context;
        this.listaCartas = listaCartas;
        System.out.println (listaCartas.size());

    }


    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.carta_layout, parent, false);
        System.out.println("Inflando cartas");
        return new ImageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, int position) {

        holder.imageView.setImageResource(listaCartas.get(position).getImage());
        System.out.println("Estoy rellenando cartas");

    }

    @Override
    public int getItemCount() {
        return listaCartas.size();
    }

    public class ImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imageView;


        public ImageHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            imageView.setOnClickListener(this);




        }

        @Override
        public void onClick(View view) {

            listaCartas.get(getAdapterPosition()).girar();
            notifyDataSetChanged();

        }
    }
}
